#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

"""
Note: The above text contains copyright information and name of the original authors, 
from whom I adapted the code below.
"""

# Importing Python 2/3 Compatibility functions
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True

class MoveGroupPythonInterfaceTutorial(object):

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        ## First initializing `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiating a `RobotCommander` object. This provides information such as the robot's
        ## kinematic model and the robot's current joint states.
        robot = moveit_commander.RobotCommander()

        ## Instantiating a `PlanningSceneInterface` object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiating a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints). This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        ## Getting Basic Information of the Robot

        # Frame of reference of the robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # Name of the End-effector for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # List of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Entire state of the robot (Useful for debugging):
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
    

        # Miscellaneous variables:
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self):
        
        move_group = self.move_group

        ## Planning to a Joint Goal
        ## The UR-5's zero/initial configuration is at a `singularity.
        ## So, the first thing we want to do is move it to a slightly better configuration. 
        ## We can call this the "Default/Home Position".
        ## We use the constant 'tau = 2*pi' for convenience. So, tau would be an entire 360 degree rotation.
        # We get the joint values from the group and change some of the values:
        
        joint_goal = move_group.get_current_joint_values()
        
        joint_goal[0] = 0         # Shoulder Pan-Joint
        joint_goal[1] = -tau/4    # Shoulder Lift-Joint
        joint_goal[2] = tau/8     # Elbow Joint
        joint_goal[3] = -tau/8    # Wrist Joint 1
        joint_goal[4] = 0         # Wrist Joint 2
        joint_goal[5] = tau/2     # Wrist Joint 3

        ## This code was originally written for a Panda Arm which  has 7 joint parameters,
        ## But I have deleted the last one since the UR-5 has only 6.
        ## Printing the current joint goal for debugging purposes.
        print("Joint Goal = ", joint_goal)

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling "stop()"" ensures that there is no residual movement
        move_group.stop()

        # For testing:
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def go_to_pose_goal(self, x, y, z, w):
        
        move_group = self.move_group

        ## Planning to a Pose Goal
        ## We can plan a motion for this group to a desired pose for the end-effector:
        
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = w
        pose_goal.position.x = x
        pose_goal.position.y = y
        pose_goal.position.z = z

        move_group.set_pose_target(pose_goal)

        ## Now, we call the planner to compute the plan and execute it.
        # `go()` returns a boolean indicating whether the planning and execution was successful.
        success = move_group.go(wait=True)
        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets().
        move_group.clear_pose_targets()


        # For testing:
        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)

    def plan_cartesian_path(self, scale=1):
        
        move_group = self.move_group

        
        ## Planning Cartesian Paths
        ## We plan a Cartesian path directly by specifying a list of waypoints within the trajectory
        ## for the end-effector to go through. If executing  interactively in a
        ## Python shell, set scale = 1.0.
        waypoints = []

        wpose = move_group.get_current_pose().pose
        print("Initial Pose:", wpose)

        wpose.position.z -= scale * 0.2  # First move down (z) by 0.2 to create first line in "P"
        waypoints.append(copy.deepcopy(wpose))
        # Defining a variable 'curr_pose' to hold the position and orientation values of the robot's current pose.
        curr_pose = self.move_group.get_current_pose().pose 
        print("Second Pose:", curr_pose)

        wpose.position.z += scale * 0.2  # Second move up (z) by 0.2, retracing the line made before. 
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Third Pose:", curr_pose)

        wpose.position.x += scale * 0.1  # Third move right in (x) by 0.1 to make the upper portion of 'P'
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Fourth Pose:", curr_pose)

        wpose.position.z -= scale * 0.1  # Fourth move down (z) by 0.1 to make the right portion of 'P'
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Fifth Pose:", curr_pose)

        wpose.position.x -= scale * 0.1  # Fifth move left in (x) by 0.1 to make the bottom portion of 'P'
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Sixth Pose:", curr_pose)


        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space.
        
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction


    def execute_plan(self, plan):
        
        move_group = self.move_group

        
        ## Executing a Plan 
        ## Use execute if you would like the robot to follow
        ## the plan that has already been computed in cartesian function above:
        move_group.execute(plan, wait=True)

## Defining a main function to execute the above function in a sequence that we want.

def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("Welcome to Paavana's Homework 6 Submission: We will be controlling a UR5 Robot to write the letter 'P' in space.")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time.")
        print("")
        input(
            "============ Press `Enter` to set-up the Moveit_Commander ..."
        )
        tutorial = MoveGroupPythonInterfaceTutorial()

        input(
            "============ Press `Enter` to execute a movement using a joint state goal ..."
        )
        print("Moving to the Home Position...")
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to plan and display a Cartesian path to draw the letter 'P' ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_path()

        input("============ Press `Enter` to execute the saved path ...")
        tutorial.execute_plan(cartesian_plan)

        print("Cartesian Path Complete! Did you notice we drew a 'P' in space?")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()


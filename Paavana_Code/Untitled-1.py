# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# Initializing moveit_commander and a rospy node

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

# Instantiating RobotCommander object since this contains information about the robot's kinematic model and current joint states
robot = moveit_commander.RobotCommander()

# Instantiating PlanningSceneInterface. This is a remote interface for getting, setting and updating the robot's internal understanding of the world.
scene = moveit_commander.PlanningSceneInterface()

# Instantiating MoveGroupCommander Object for the UR5 robot.
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# To display trajectories in RViz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,)

# Getting basic information about the robot
# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())


print("============ Printing robot state")
print(robot.get_current_state())
print("")

# Since the robot may have a singularity in the initial position, I am moving the robot to a slightly better position
# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau/6
joint_goal[2] = tau/8
joint_goal[3] = tau/8
joint_goal[4] = 0
joint_goal[5] = tau/2 
print(joint_goal)


# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()


#!/usr/bin/env python

# Software License Agreement (BSD License)
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

"""
Note: The above text contains copyright information and name of the original authors, 
from whom I adapted the code below.
"""

# Importing Python 2/3 Compatibility functions
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True

class MoveGroupPythonInterfaceTutorial(object):

    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

        # First initializing `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        # Instantiating a `RobotCommander` object. This provides information such as the robot's
        # kinematic model and the robot's current joint states.
        robot = moveit_commander.RobotCommander()

        # Instantiating a `PlanningSceneInterface` object.  This provides a remote interface
        # for getting, setting, and updating the robot's internal understanding of the surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        # Instantiating a `MoveGroupCommander`_ object.  This object is an interface
        # to a planning group (group of joints). This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Creating a `DisplayTrajectory` ROS publisher which is used to display trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # Getting Basic Informccaation of the Robot

        # Frame of reference of the robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # Name of the End-effector for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # List of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Entire state of the robot (Useful for debugging):
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
    
        # Miscellaneous variables:
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    
    def go_to_joint_state(self):
        
        move_group = self.move_group

        # Planning to a particular Joint Goal
        # The UR-5's zero/initial configuration is at a `singularity.
        # So, the first thing we want to do is move it to a slightly better configuration. 
        # We can call this the "Default/Home Position".
        # We use the constant 'tau = 2*pi' for convenience. So, tau would be an entire 360 degree rotation.
        # We get the joint values from the group and change some of the values:
        
        joint_goal = move_group.get_current_joint_values()
        
        joint_goal[0] = 0         # Shoulder Pan-Joint
        joint_goal[1] = -tau/4    # Shoulder Lift-Joint
        joint_goal[2] = tau/8     # Elbow Joint
        joint_goal[3] = -tau/8    # Wrist Joint 1
        joint_goal[4] = 0         # Wrist Joint 2
        joint_goal[5] = tau/2     # Wrist Joint 3
        # This code was originally written for a Panda Arm which  has 7 joint parameters,
        # But I have deleted the last one since the UR-5 has only 6.
        
        # Printing the current joint goal for debugging purposes.
        print("Joint Goal = ", joint_goal)

        # The 'go' command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling "stop()" ensures that there is no residual movement
        move_group.stop()

        # Printing the current joint values for the calculations of my FIRST POSE
        print("POSE 1:")
        print("The current joint values are: \n")
        current_joints = move_group.get_current_joint_values()
        print(current_joints)
        
        return all_close(joint_goal, current_joints, 0.01)

    # This function contains the cartesian movements needed to draw the letter "P" in space. 
    # I chose "P" for "Paavana" 
    def plan_cartesian_P(self, scale=1):
        
        move_group = self.move_group

        # Planning Cartesian Paths is done by directly by specifying a list of waypoints 
        # within the trajectory for the end-effector to go through. 
        # If executing interactively in a Python shell, set scale = 1.0.
        # Creating an empty array which will hold the values of the waypoints.
        waypoints = []

        wpose = move_group.get_current_pose().pose
        print("Initial Pose:", wpose)

        # First: Move down the z-axis by 0.2 units to create first line in "P"
        wpose.position.z -= scale * 0.2  
        waypoints.append(copy.deepcopy(wpose))
        # For debugging and general information purposes:
        # Defining a variable 'curr_pose' to hold the position and orientation values of the robot's current pose.
        curr_pose = self.move_group.get_current_pose().pose 
        print("Second Pose:", curr_pose)

        # Second: Move up the z-axis by 0.2 units, retracing the line made before. 
        wpose.position.z += scale * 0.2  
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Third Pose:", curr_pose)

        # Third: Move right in the x-axis by 0.1 units to make the upper portion of 'P'
        wpose.position.x += scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Fourth Pose:", curr_pose)

        # Fourth: move down the z-axis by 0.1 units to make the right portion of 'P'
        wpose.position.z -= scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Fifth Pose:", curr_pose)

        # Fifth move left in x-axis by 0.1 units to make the bottom portion of 'P'
        wpose.position.x -= scale * 0.15  
        waypoints.append(copy.deepcopy(wpose))
        curr_pose = self.move_group.get_current_pose().pose
        print("Sixth Pose:", curr_pose)

        # We want the Cartesian path to be interpolated at a resolution of 10 cm
        # which is why we will specify 0.1 as the End-effector_step in Cartesian translation. 
        # We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space.
        
        (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.08, 0.0)  
        # waypoints to follow  # eef-step = 0.1 # jump_threshold = 0.0

        # Note: We are just planning, not asking move_group to actually move the robot yet
        return plan, fraction

    
    # This function contains the cartesian movements needed to draw the letter "K" in space.
    # I chose "K" for "Kamisetty"
    def plan_cartesian_K(self, scale=1):
        
        move_group = self.move_group
        waypoints = []

        wpose = move_group.get_current_pose().pose
        print("Initial Pose S:", wpose)

        # First: move down in z-axis by 0.2 units to create the first line in "K"
        wpose.position.z -= scale * 0.2  
        waypoints.append(copy.deepcopy(wpose))
        
        # Second: move up the z-axis by 0.1 units to reach the midpoint of the line made in step 1. 
        wpose.position.z += scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        
        # Third: move to the upper right corner to make the upper portion of 'K'
        wpose.position.x += scale * 0.1  
        wpose.position.z += scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))

        # Printing the current joint values for the calculations of my SECOND POSE
        print("POSE 2:")
        print("The current joint values are: \n")
        current_joints = move_group.get_current_joint_values()
        print(current_joints)
        
        # Fourth: move back to midpoint of vertical line by retracing the previous step.
        wpose.position.z -= scale * 0.1  
        wpose.position.x -= scale * 0.1 
        waypoints.append(copy.deepcopy(wpose))
        
        # Fifth: move to the lower right corner to make the bottom portion of 'K'
        wpose.position.x += scale * 0.1  
        wpose.position.z -= scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        
        
        (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.08, 0.0)  
        # Waypoints to Follow  # eef-step = 0.08 # jump_threshold = 0.0

        return plan, fraction

    # This function contains the cartesian movements needed to draw the letter "S" in space.
    # I chose "S" for "Srinivas"
    def plan_cartesian_S(self, scale=1):
        
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        print("Initial Pose:", wpose)

        # First move in x-axis by 0.1 units to create first top line in "S"
        wpose.position.x -= scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        
        # Second move down in z-axis by 0.1 to make second line in 'S'
        wpose.position.z -= scale * 0.1   
        waypoints.append(copy.deepcopy(wpose))
        
        # Third move right in x-axis by 0.1 to make the third line in 'S'
        wpose.position.x += scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        
        # Fourth move down the z-axis by 0.1 units to make the fourth line in 'S'
        wpose.position.z -= scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))
        
        # Fifth move left in x-axis by 0.1 units to make the bottom portion of 'S'
        wpose.position.x -= scale * 0.1  
        waypoints.append(copy.deepcopy(wpose))

        # Printing the current joint values for the calculations of my THIRD POSE
        print("POSE 3:")
        print("The current joint values are: \n")
        current_joints = move_group.get_current_joint_values()
        print(current_joints)

        
        (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.08, 0.0)  
        # Waypoints to follow  # eef_step = 0.1 # jump_threshold = 0.0

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction
    

    # This function is used to execute the cartesian functions computed above.
    def execute_plan(self, plan):
        
        move_group = self.move_group

        move_group.execute(plan, wait=True)


# Defining a main function to execute the above functions in a sequence that we want.
def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("Welcome to Paavana's Midterm Submission:")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time.")
        print("")

        input("Press 'Enter' to set-up the Moveit_Commander ...")
        tutorial = MoveGroupPythonInterfaceTutorial()

        # Home Position
        input("Press 'Enter' to execute a movement to a pre-defined joint state goal ...")
        print("Moving to the Home Position...")
        tutorial.go_to_joint_state()

        # Executing path "P"
        input("Press `Enter` to plan and draw the letter 'P' ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_P()
        tutorial.execute_plan(cartesian_plan)

        # Home Position
        input("Press 'Enter' to move back to Home Position ...")
        tutorial.go_to_joint_state()

        # Executing path "K"
        input("Press 'Enter' to plan and draw the letter 'K' ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_K()
        tutorial.execute_plan(cartesian_plan)

        # Home Position
        input("Press 'Enter' to move back to Home Position ...")
        tutorial.go_to_joint_state()

        # Executing path "S"                      
        input("Press `Enter` to plan and draw the letter 'S' ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_S()
        tutorial.execute_plan(cartesian_plan)

        # Home Position
        input("Press 'Enter' to move back to Home Position ...")
        tutorial.go_to_joint_state()

        print("Cartesian Path Complete! Did you notice we drew a 'P', 'K' and 'S' in space?")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()


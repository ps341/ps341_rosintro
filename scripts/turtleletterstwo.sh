#!/usr/bin/bash

rostopic pub -1 /Turtle1/cmd_vel geometry_msgs/Twist '[0.0, 5.0, 0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /Turtle1/cmd_vel geometry_msgs/Twist '[5.0, 0.0, 0.0]' '[0.0,0.0, -3.3]'

rostopic pub -1 /Turtle2/cmd_vel geometry_msgs/Twist '[8.0, 0.0, 0.0]' '[0.0, 0.0,8.0]'

rostopic pub -1 /Turtle2/cmd_vel geometry_msgs/Twist '[0.0,0.0,0.0]' '[0.0,0.0,-0.22]'

rostopic pub -1 /Turtle2/cmd_vel geometry_msgs/Twist '[0.8,0.0,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /Turtle2/cmd_vel geometry_msgs/Twist '[-2.5,0.0,0.0]' '[0.0,0.0,0.0]'
